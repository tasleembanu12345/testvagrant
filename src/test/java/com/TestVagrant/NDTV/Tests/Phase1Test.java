package com.TestVagrant.NDTV.Tests;

import org.testng.annotations.Test;

import com.TestVagrant.NDTV.Core.BaseTest;
import com.TestVagrant.NDTV.Pages.Navigate;

public class Phase1Test extends BaseTest {

	@Test(description = "Validating phase 1 Scenario", enabled = true)
	public void phase_1_Test() throws InterruptedException {
		Navigate nav = new Navigate();
		nav.navigateToWether();
	}

}
