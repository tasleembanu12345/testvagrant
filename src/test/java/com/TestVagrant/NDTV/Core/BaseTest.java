package com.TestVagrant.NDTV.Core;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.TestVagrant.NDTV.Utiities.Utils;

import sun.security.util.Cache;

public class BaseTest {
	public static WebDriver driver;
	static Utils utils = new Utils();

	@BeforeSuite
	public void start() {
		initialize();
	}

	public WebDriver getDriver() {
		return driver;
	}

	public static WebDriver initialize() {
		String browser = utils.getConfigPropertyValues("BROWSER");

		if (browser.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./Drivers/" + "chromedriver.exe");
			ChromeOptions options=new ChromeOptions();
			Map prefs=new HashMap();
			prefs.put("profile.default_content_setting_values.notifications", 1);
			options.setExperimentalOption("prefs",prefs);
	        driver=new ChromeDriver(options);
	        driver.manage().deleteAllCookies();

		} else {

			System.setProperty("webdriver.gecko.driver", "./Drivers/" + "geckodriver.exe");
			driver = new FirefoxDriver();

		}
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get(utils.getConfigPropertyValues("URL"));
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		return driver;
	}

	@AfterSuite
	public void closeAll() {
		driver.quit();
	}

}
