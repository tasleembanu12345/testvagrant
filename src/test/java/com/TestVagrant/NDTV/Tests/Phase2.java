package com.TestVagrant.NDTV.Tests;

import org.testng.annotations.Test;

import com.TestVagrant.NDTV.Core.BaseAPI;
import com.TestVagrant.NDTV.Utiities.AppConstants;
import com.TestVagrant.NDTV.Utiities.Utils;

import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;

public class Phase2 extends BaseAPI {

	@Test(priority = 1, description = "current weather", enabled = true)
	public void getCurrentWeatherTest() {
		Utils utils = new Utils();
		given().header(AppConstants.API_KEY, HEADER)
				.param(utils.getConfigPropertyValues("PARAMKEY"), utils.getConfigPropertyValues("PARAMVALUE")).log()
				.all().when().get(utils.getConfigPropertyValues("ENDPOINT")).then().assertThat().statusCode(200).log()
				.all().contentType(ContentType.JSON).log().all();

	}

}
