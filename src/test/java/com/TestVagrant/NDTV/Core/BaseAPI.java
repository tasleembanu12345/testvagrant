package com.TestVagrant.NDTV.Core;

import org.testng.annotations.BeforeSuite;

import com.TestVagrant.NDTV.Utiities.Utils;

import io.restassured.RestAssured;

public class BaseAPI {
	Utils utils = new Utils();
	public static String HEADER;

	@BeforeSuite
	public void beforeSuite() {
		RestAssured.baseURI = utils.getConfigPropertyValues("DOMAIN");
		HEADER = utils.getConfigPropertyValues("XAPIKEY");

	}

}
