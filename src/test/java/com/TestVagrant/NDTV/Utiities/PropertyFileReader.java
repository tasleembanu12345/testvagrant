package com.TestVagrant.NDTV.Utiities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertyFileReader {
	private Properties props = null;

	public Properties getAllProperties(String propertyFileName) throws FileNotFoundException, IOException {

		File configFile = new File(System.getProperty("user.dir")
				+ "/src/test/java/com/TestVagrant/NDTV/Utiities/config.properties");
		props = new Properties();
		props.load(new FileInputStream(configFile));
		return props;

	}

}
